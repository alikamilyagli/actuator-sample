FROM maven:3.6.3-jdk-8
VOLUME /tmp
RUN echo $(ls /target)
ADD /target/actuator-sample-0.0.1.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
